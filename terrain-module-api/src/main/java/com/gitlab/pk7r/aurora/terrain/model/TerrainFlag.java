package com.gitlab.pk7r.aurora.terrain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "terrain_flag")
public class TerrainFlag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "flag", nullable = false, columnDefinition = "smallint")
    private Flag flag;

    @Column(name = "value", nullable = false)
    private String value;

    @ManyToOne
    private Terrain terrain;

}
