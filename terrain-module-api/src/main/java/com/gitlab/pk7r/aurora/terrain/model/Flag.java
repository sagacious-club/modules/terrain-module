package com.gitlab.pk7r.aurora.terrain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Flag {

    PVP(FlagType.BOOLEAN, "o pvp"), MOB(FlagType.BOOLEAN, "o spawn de mobs"), ENTRY(FlagType.BOOLEAN, "a entrada"), GREETING(FlagType.STRING, "a mensagem de entrada"), FAREWELL(FlagType.STRING, "a mensagem de saída");

    private final FlagType flagType;

    private final String description;

}