package com.gitlab.pk7r.aurora.terrain.model;

public enum FlagType {

    STRING, BOOLEAN

}