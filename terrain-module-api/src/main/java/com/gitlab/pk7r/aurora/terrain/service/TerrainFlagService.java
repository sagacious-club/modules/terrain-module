package com.gitlab.pk7r.aurora.terrain.service;

import com.gitlab.pk7r.aurora.terrain.model.Flag;
import com.gitlab.pk7r.aurora.terrain.model.FlagType;
import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.model.TerrainFlag;
import org.springframework.data.util.Pair;

import java.util.List;

public interface TerrainFlagService {

    void setDefaultFlags(Terrain terrain);

    void updateTerrainFlag(Terrain terrain, Flag flag, String value);

    List<TerrainFlag> getTerrainFlags(Terrain terrain);

    boolean getPvPFlag(Terrain terrain);

    boolean getMobSpawnFlag(Terrain terrain);

    boolean getEntryFlag(Terrain terrain);

    String getGreetingFlag(Terrain terrain);

    String getFarewellFlag(Terrain terrain);

}
