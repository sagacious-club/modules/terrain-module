package com.gitlab.pk7r.aurora.terrain.service;

import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public interface TerrainService {

    void createTerrain(Player player, String owner, Location location);

    void expandTerrain(Player player);

    void updateTerrainHome(Terrain terrain, Location location);

    List<Terrain> getTerrains();

    Optional<Terrain> getTerrain(Integer id);

    Optional<Terrain> getPlayerTerrain(Player player);

    Optional<Terrain> getPlayerTerrain(String player);

    Optional<Terrain> getTerrainByChunk(String chunk);

    boolean isTerrain(String chunk, boolean concurrent);

    boolean hasTerrain(Player player);

    boolean hasTerrain(String player);

    boolean inTerrainsWorld(Player player);

    void showBorder(Player player, Terrain terrain, Particle particle, int timeInSeconds);

    boolean hasTerrainAround(String chunks);

    boolean hasTerrainAround(String chunks, String owner);

    void deleteTerrain(Player player);

    void setTerrainOwner(Terrain terrain, String newOwner);

}