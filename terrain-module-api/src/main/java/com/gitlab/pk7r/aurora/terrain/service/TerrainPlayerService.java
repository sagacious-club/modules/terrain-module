package com.gitlab.pk7r.aurora.terrain.service;

import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.model.TerrainPlayer;
import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public interface TerrainPlayerService {

    void setPlayerTrustState(Integer terrainId, Player player, TrustState trustState);

    List<TerrainPlayer> getTerrainPlayers(Terrain terrain);

    Optional<TerrainPlayer> getTerrainPlayer(Terrain terrain, Player player);

    boolean isMember(Terrain terrain, Player player);

    boolean isTrusted(Terrain terrain, Player player);

    boolean isBanned(Terrain terrain, Player player);

    boolean isOwner(Terrain terrain, Player player);

}