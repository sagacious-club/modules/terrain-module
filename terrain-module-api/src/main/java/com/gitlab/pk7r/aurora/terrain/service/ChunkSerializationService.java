package com.gitlab.pk7r.aurora.terrain.service;

import org.bukkit.Chunk;

import java.util.List;

public interface ChunkSerializationService {

    String serializeChunk(Chunk chunk);

    String serializeChunks(Chunk... chunks);

    String serializeChunks(List<Chunk> chunks);

    Chunk deserializeChunk(String chunk);

    List<Chunk> deserializeChunks(String chunks);

}