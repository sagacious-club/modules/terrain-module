package com.gitlab.pk7r.aurora.terrain.event;

import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TerrainPlayerUpdateTrustStateEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    @Setter
    @Getter
    private boolean cancelled;

    @Getter
    private final Terrain terrain;

    @Getter
    private final Player player;

    @Getter
    private final TrustState oldTrustState;

    @Getter
    private final TrustState newTrustState;

    public TerrainPlayerUpdateTrustStateEvent(Terrain terrain, Player player, TrustState oldTrustState, TrustState newTrustState) {
        this.cancelled = false;
        this.terrain = terrain;
        this.player = player;
        this.oldTrustState = oldTrustState;
        this.newTrustState = newTrustState;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
