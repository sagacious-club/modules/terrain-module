package com.gitlab.pk7r.aurora.terrain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "terrain_player")
public class TerrainPlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username", nullable = false)
    private String username;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "trust_state", nullable = false, columnDefinition = "smallint")
    private TrustState trustState = TrustState.UNTRUSTED;

    @ManyToOne
    private Terrain terrain;

}