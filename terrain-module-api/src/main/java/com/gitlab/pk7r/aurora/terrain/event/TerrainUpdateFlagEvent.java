package com.gitlab.pk7r.aurora.terrain.event;

import com.gitlab.pk7r.aurora.terrain.model.Flag;
import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TerrainUpdateFlagEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    @Setter
    @Getter
    private boolean cancelled;

    @Getter
    private final Terrain terrain;

    @Getter
    private final Flag flag;

    @Getter
    private final String value;

    public TerrainUpdateFlagEvent(boolean async, Terrain terrain, Flag flag, String value) {
        super(async);
        this.cancelled = false;
        this.terrain = terrain;
        this.flag = flag;
        this.value = value;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}

