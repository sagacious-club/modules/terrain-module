package com.gitlab.pk7r.aurora.terrain.model;

public enum TrustState {

    BANNED, UNTRUSTED, PARTIAL, TOTAL

}
