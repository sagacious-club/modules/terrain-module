package com.gitlab.pk7r.aurora.terrain.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "terrain")
public class Terrain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "owner", nullable = false)
    private String owner;

    @Column(name = "center", nullable = false)
    private String center;

    @Column(name = "home", nullable = false)
    private String home;

    @Column(name = "chunks", columnDefinition = "LONGTEXT", nullable = false)
    private String chunks;

    @Column(name = "expansions", nullable = false)
    private Integer expansions;

    @Column(name = "claim_date", nullable = false)
    private LocalDateTime claimDate = LocalDateTime.now();

    @OneToMany(mappedBy = "terrain", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<TerrainPlayer> players;

    @OneToMany(mappedBy = "terrain", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<TerrainFlag> flags;

}