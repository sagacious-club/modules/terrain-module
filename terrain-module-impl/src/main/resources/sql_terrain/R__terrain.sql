CREATE TABLE IF NOT EXISTS terrain (
    id         INT AUTO_INCREMENT NOT NULL,
    owner      VARCHAR(255)       NOT NULL,
    center     VARCHAR(255)       NOT NULL,
    home       VARCHAR(255)       NOT NULL,
    chunks     LONGTEXT           NOT NULL,
    expansions INT                NOT NULL,
    claim_date datetime           NOT NULL,
    CONSTRAINT pk_terrain PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS terrain_player (
    id          INT AUTO_INCREMENT NOT NULL,
    username    VARCHAR(255)       NOT NULL,
    trust_state SMALLINT           NOT NULL,
    terrain_id  INT                NULL,
    CONSTRAINT pk_terrain_player PRIMARY KEY (id)
);

ALTER TABLE terrain_player ADD CONSTRAINT FK_TERRAIN_PLAYER_ON_TERRAIN FOREIGN KEY (terrain_id) REFERENCES terrain (id);

CREATE TABLE IF NOT EXISTS terrain_flag (
    id         INT AUTO_INCREMENT NOT NULL,
    flag       SMALLINT           NOT NULL,
    value      VARCHAR(255)       NOT NULL,
    terrain_id INT                NULL,
    CONSTRAINT pk_terrain_flag PRIMARY KEY (id)
);

ALTER TABLE terrain_flag ADD CONSTRAINT FK_TERRAIN_FLAG_ON_TERRAIN FOREIGN KEY (terrain_id) REFERENCES terrain (id);