package com.gitlab.pk7r.aurora.terrain.command.subcommands.admin;

import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class TerrainAdminSwitchCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainPlayerService terrainPlayerService, ChunkSerializationService chunkSerializationService) {
        return new CommandAPICommand("switch")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.switch")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    if (!terrainService.hasTerrain(target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador não tem um terreno.").formatted());
                        return;
                    }
                    val chunk = chunkSerializationService.serializeChunk(player.getLocation().getChunk());
                    if (!terrainService.isTerrain(chunk, true)) {
                        player.spigot().sendMessage(new Message("&cVocê não está em um terreno.").formatted());
                        return;
                    }
                    val targetTerrain = terrainService.getPlayerTerrain(target).orElseThrow();
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    if (terrainPlayerService.isOwner(terrain, target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador já é dono deste terreno.").formatted());
                        return;
                    }
                    val oldOwner = terrain.getOwner();
                    terrainService.setTerrainOwner(terrain, String.format("%s:%s", oldOwner, targetTerrain.getOwner()));
                    terrainService.setTerrainOwner(targetTerrain, String.format("%s:%s", targetTerrain.getOwner(), oldOwner));
                    terrainService.setTerrainOwner(terrain, target.getName().toLowerCase());
                    terrainService.setTerrainOwner(targetTerrain, oldOwner);
                    player.spigot().sendMessage(new Message(String.format("&aVocê trocou o terreno de &e%s &acom &e%s&a.", oldOwner, terrain.getOwner())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("switch")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.switch")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno admin switch <jogador>").formatted());
                });
    }
}
