package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.terrain.util.ChunkUtil;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.val;
import org.bukkit.Chunk;
import org.bukkit.Particle;

public class TerrainClaimCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, ChunkSerializationService chunkSerializationService) {
        return new CommandAPICommand("coletar")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.coletar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (terrainService.inTerrainsWorld(player)) {
                        player.spigot().sendMessage(new Message("&cVocê precisa estar no mundo dos terrenos para executar este comando.").formatted());
                        return;
                    }
                    if (terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê já tem um terreno.").formatted());
                        return;
                    }
                    val location = player.getLocation();
                    val centerChunk = location.getChunk();
                    val terrainChunks = ChunkUtil.around(centerChunk, 2);
                    terrainChunks.add(centerChunk);
                    if (terrainService.hasTerrainAround(chunkSerializationService.serializeChunks(terrainChunks.toArray(Chunk[]::new)))) {
                        terrainChunks.stream().filter(chunk -> terrainService.isTerrain(chunkSerializationService.serializeChunk(chunk), true))
                                .map(chunkSerializationService::serializeChunk)
                                .forEach(chunk -> {
                                    val terrain = terrainService.getTerrainByChunk(chunk);
                                    terrain.ifPresent(value -> terrainService.showBorder(player, value, Particle.VILLAGER_ANGRY, 3));
                                });
                        player.spigot().sendMessage(new Message("&cEsta localização está muito próxima de outro terreno.").formatted());
                        return;
                    }
                    terrainService.createTerrain(player, player.getName(), location);
                    player.spigot().sendMessage(new Message("&aVocê coletou um terreno com sucesso.").formatted());
                });
    }
}