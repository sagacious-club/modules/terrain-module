package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.service.DelayService;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.val;

import java.util.concurrent.TimeUnit;

public class TerrainExpandCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainPlayerService terrainPlayerService, ChunkSerializationService chunkSerializationService, DelayService delayService) {
        return new CommandAPICommand("expandir")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.expandir")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (terrainService.inTerrainsWorld(player)) {
                        player.spigot().sendMessage(new Message("&cVocê precisa estar no mundo dos terrenos para executar este comando.").formatted());
                        return;
                    }
                    val chunk = chunkSerializationService.serializeChunk(player.getLocation().getChunk());
                    if (!terrainService.isTerrain(chunk, false)) {
                        player.spigot().sendMessage(new Message("&cVocê não está em um terreno.").formatted());
                        return;
                    }
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    if (!terrainPlayerService.isOwner(terrain, player)) {
                        player.spigot().sendMessage(new Message("&cVocê não está no seu terreno.").formatted());
                        return;
                    }
                    if (!delayService.assertDelay(player, player.getName() + ":terrain-expand", 180, TimeUnit.SECONDS)) {
                        terrainService.expandTerrain(player);
                    }
                });
    }
}
