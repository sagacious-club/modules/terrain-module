package com.gitlab.pk7r.aurora.terrain.repository;

import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TerrainRepository extends JpaRepository<Terrain, Integer> {

    Optional<Terrain> findByOwner(String owner);

    Optional<Terrain> findTerrainByChunksContaining(String chunks);

    boolean existsByChunksContaining(String chunks);

    boolean existsByChunksContainingAndOwnerIsNot(String chunks, String owner);

    boolean existsByOwner(String owner);

}