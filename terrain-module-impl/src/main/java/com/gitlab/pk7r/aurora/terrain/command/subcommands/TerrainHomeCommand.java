package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.service.LocationSerializationService;
import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.StringArgument;
import lombok.val;

public class TerrainHomeCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, LocationSerializationService locationSerializationService) {
        return new CommandAPICommand("home")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.home")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                        return;
                    }
                    val terrain = terrainService.getPlayerTerrain(player).orElseThrow();
                    val homeLocation = locationSerializationService.deserializeLocation(terrain.getHome());
                    player.teleport(homeLocation);
                    player.spigot().sendMessage(new Message("&aVocê teletransportou para o seu terreno.").formatted());
                });
    }

    public static CommandAPICommand getCompleteCommand(TerrainService terrainService, LocationSerializationService locationSerializationService, TerrainPlayerService terrainPlayerService) {
        return new CommandAPICommand("home")
                .withArguments(new StringArgument("jogador").replaceSuggestions(ArgumentSuggestions.strings(info -> terrainService.getTerrains().stream().map(Terrain::getOwner).toArray(String[]::new))))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.home.others")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val target = (String) args[0];
                    if (!terrainService.hasTerrain(target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador não tem um terreno.").formatted());
                        return;
                    }
                    val terrain = terrainService.getPlayerTerrain(target).orElseThrow();
                    if (terrainPlayerService.isBanned(terrain, player) && !player.hasPermission("aurora.terrain.bypass.ban")) {
                        player.spigot().sendMessage(new Message("&cVocê está banido deste terreno.").formatted());
                        return;
                    }
                    val homeLocation = locationSerializationService.deserializeLocation(terrain.getHome());
                    player.teleport(homeLocation);
                    player.spigot().sendMessage(new Message(String.format("&aTeletransportado para o terreno de &e%s&a.", terrain.getOwner())).formatted());
                });
    }

}
