package com.gitlab.pk7r.aurora.terrain.listener;

import com.gitlab.pk7r.aurora.Event;
import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import com.gitlab.pk7r.aurora.terrain.event.TerrainPlayerUpdateTrustStateEvent;
import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainFlagService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.Plugin;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@Event
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TerrainListener implements Listener {

    Plugin plugin;

    TerrainService terrainService;

    TerrainPlayerService terrainPlayerService;

    TerrainFlagService terrainFlagService;
    
    FileConfiguration fileConfiguration;

    ChunkSerializationService chunkSerializationService;

    @EventHandler
    public void onBan(TerrainPlayerUpdateTrustStateEvent event) {
        val terrain = event.getTerrain();
        val player = event.getPlayer();
        if (event.getNewTrustState().equals(TrustState.BANNED)) {
            if (player.hasPermission("aurora.terrain.bypass.ban")) {
                return;
            }
            player.spigot().sendMessage(new Message(String.format("&cVocê foi banido do terreno de &e%s&c.", terrain.getOwner())).formatted());
            if (chunkSerializationService.deserializeChunks(terrain.getChunks()).stream().anyMatch(chunk -> chunk.equals(player.getLocation().getChunk()))) {
                player.performCommand("spawn");
            }
        }
    }
    
    @EventHandler
    public void onBlockExplosion(BlockExplodeEvent event) {
        if (notInTerrainsWorld(event.getBlock().getLocation())) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onEntityExplosion(EntityExplodeEvent event) {
        if (notInTerrainsWorld(event.getLocation())) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onMobSpawn(CreatureSpawnEvent event) {
        if (notInTerrainsWorld(event.getLocation())) return;
        val chunk = chunkSerializationService.serializeChunk(event.getLocation().getChunk());
        if (terrainService.isTerrain(chunk, false)) {
            if (terrainService.getTerrainByChunk(chunk).isEmpty()) return;
            val terrain = terrainService.getTerrainByChunk(chunk).get();
            event.setCancelled(!terrainFlagService.getMobSpawnFlag(terrain));
        }
    }

    @EventHandler
    public void onCombat(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player damager) {
            if (event.getEntity() instanceof Player damaged) {
                if (notInTerrainsWorld(damager.getLocation())) return;
                val chunk = chunkSerializationService.serializeChunk(damaged.getLocation().getChunk());
                if (!terrainService.isTerrain(chunk, false)) return;
                if (terrainService.getTerrainByChunk(chunk).isEmpty()) return;
                val terrain = terrainService.getTerrainByChunk(chunk).get();
                event.setCancelled(!terrainFlagService.getPvPFlag(terrain));
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(PlayerMoveEvent event) {
        val player = event.getPlayer();
        if (notInTerrainsWorld(player.getLocation())) return;
        if (event.getTo() != null) {
            if (event.getFrom().getBlockX() != event.getTo().getBlockX() ||
                    event.getFrom().getBlockY() != event.getTo().getBlockY() ||
                    event.getFrom().getBlockZ() != event.getTo().getBlockZ()) {
                val from = terrainService.isTerrain(chunkSerializationService.serializeChunk(event.getFrom().getChunk()), false);
                val to = terrainService.isTerrain(chunkSerializationService.serializeChunk(event.getTo().getChunk()), false);
                if (from && !to) {
                    val chunk = chunkSerializationService.serializeChunk(event.getFrom().getChunk());
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    terrainLeaveProceed(terrain, player);
                }
                if (!from && to) {
                    val chunk = chunkSerializationService.serializeChunk(event.getTo().getChunk());
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    terrainJoinProceed(terrain, player, event);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onTeleport(PlayerTeleportEvent event) {
        val player = event.getPlayer();
        if (notInTerrainsWorld(player.getLocation())) return;
        if (event.getTo() != null) {
            if (event.getFrom().getBlockX() != event.getTo().getBlockX() ||
                    event.getFrom().getBlockY() != event.getTo().getBlockY() ||
                    event.getFrom().getBlockZ() != event.getTo().getBlockZ()) {
                val from = terrainService.isTerrain(chunkSerializationService.serializeChunk(event.getFrom().getChunk()), false);
                val to = terrainService.isTerrain(chunkSerializationService.serializeChunk(event.getTo().getChunk()), false);
                if (from && !to) {
                    val chunk = chunkSerializationService.serializeChunk(event.getFrom().getChunk());
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    terrainLeaveProceed(terrain, player);
                }
                if (!from && to) {
                    val chunk = chunkSerializationService.serializeChunk(event.getTo().getChunk());
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    terrainJoinProceed(terrain, player, event);
                }
            }
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        val player = event.getPlayer();
        if (notInTerrainsWorld(player.getLocation())) return;
        val block = event.getBlockPlaced();
        if (block.getType().equals(Material.TNT) || block.getType().equals(Material.TNT_MINECART))
            event.setCancelled(true);
        interactionEventHandler(player, event, block);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        val player = event.getPlayer();
        if (notInTerrainsWorld(player.getLocation())) return;
        val block = event.getBlock();
        interactionEventHandler(player, event, block);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        val player = event.getPlayer();
        if (notInTerrainsWorld(player.getLocation())) return;
        if (event.getClickedBlock() != null) {
            val block = event.getClickedBlock();
            interactionEventHandler(player, event, block);
        }
    }

    private void interactionEventHandler(Player player, Cancellable event, Block block) {
        val chunk = chunkSerializationService.serializeChunk(block.getChunk());
        if (!terrainService.isTerrain(chunk, false)) {
            if (!player.hasPermission("aurora.terrain.bypass.protection.world")) {
                player.spigot().sendMessage(new Message("&cVocê não pode quebrar blocos fora do terreno.").formatted());
            }
            event.setCancelled(!player.hasPermission("aurora.terrain.bypass.protection.world"));
        } else {
            val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
            if (terrainPlayerService.isOwner(terrain, player) || terrainPlayerService.isTrusted(terrain, player) ||
                    player.hasPermission("aurora.terrain.bypass.protection.terrain") || terrainPlayerService.isMember(terrain, player)) {
                val isOwnerOnline = Bukkit.getOnlinePlayers().stream().anyMatch(onlinePlayer -> onlinePlayer.getName().equalsIgnoreCase(terrain.getOwner()));
                if (!isOwnerOnline && terrainPlayerService.isMember(terrain, player)) {
                    event.setCancelled(true);
                }
            } else event.setCancelled(true);
        }
    }
    
    void terrainJoinProceed(Terrain terrain, Player player, PlayerMoveEvent event) {
        if (terrainPlayerService.isBanned(terrain, player) && !player.hasPermission("aurora.terrain.bypass.ban")) {
            event.setCancelled(true);
            player.spigot().sendMessage(new Message("&cVocê está banido deste terreno.").formatted());
            return;
        }
        val entryFlag = terrainFlagService.getEntryFlag(terrain);
        if (!entryFlag && !terrainPlayerService.isOwner(terrain, player) && !terrainPlayerService.isTrusted(terrain, player)) {
            if (terrainPlayerService.isMember(terrain, player)) {
                val isOwnerOnline = Bukkit.getOnlinePlayers().stream().anyMatch(onlinePlayer -> onlinePlayer.getName().equalsIgnoreCase(terrain.getOwner()));
                if (!isOwnerOnline) {
                    player.spigot().sendMessage(new Message("&cEste terreno não está aberto para visitas.").formatted());
                    event.setCancelled(true);
                }
            } else {
                player.spigot().sendMessage(new Message("&cEste terreno não está aberto para visitas.").formatted());
                event.setCancelled(true);
            }
            return;
        }
        if (!event.isCancelled()) {
            if (terrainFlagService.getGreetingFlag(terrain).equalsIgnoreCase("")) return;
            if (terrainFlagService.getGreetingFlag(terrain).contains(";")) {
                val greeting = terrainFlagService.getGreetingFlag(terrain).split(";");
                player.sendTitle(ChatColor.translateAlternateColorCodes('&', greeting[0]), ChatColor.translateAlternateColorCodes('&', greeting[1]), 25, 50, 25);
            } else {
                val greeting = terrainFlagService.getGreetingFlag(terrain);
                player.sendTitle(ChatColor.translateAlternateColorCodes('&', greeting), "", 25, 50, 25);
            }
        }
    }
    
    void terrainLeaveProceed(Terrain terrain, Player player) {
        if (terrainFlagService.getFarewellFlag(terrain).equalsIgnoreCase("")) return;
        if (terrainFlagService.getFarewellFlag(terrain).contains(";")) {
            val farewell = terrainFlagService.getFarewellFlag(terrain).split(";");
            player.sendTitle(ChatColor.translateAlternateColorCodes('&', farewell[0]), ChatColor.translateAlternateColorCodes('&', farewell[1]), 25, 50, 25);
        } else {
            val farewell = terrainFlagService.getFarewellFlag(terrain);
            player.sendTitle(ChatColor.translateAlternateColorCodes('&', farewell), "", 25, 50, 25);
        }
    }
    
    boolean notInTerrainsWorld(Location location) {
        return !Objects.requireNonNull(fileConfiguration.getConfiguration("terrain.yml").getString("terrains-world"))
                .equalsIgnoreCase(Objects.requireNonNull(location.getWorld()).getName());
    }
}