package com.gitlab.pk7r.aurora.terrain.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.security.SecureRandom;
import java.util.Random;

public class LocationUtil {

    public static Location genRandomLocation(World world, float yaw, float pitch) {
        Random random = new SecureRandom();

        int centerX = world.getWorldBorder().getCenter().getBlockX();
        int centerZ = world.getWorldBorder().getCenter().getBlockZ();
        int boundSize = (int) world.getWorldBorder().getSize();

        int x, y, z;
        Material blockMaterial;

        do {
            x = centerX + random.nextInt(boundSize) - boundSize/2;
            z = centerZ + random.nextInt(boundSize) - boundSize/2;

            Block topBlock = world.getHighestBlockAt(x, z);
            y = topBlock.getY() + 1;
            blockMaterial = topBlock.getType();
        } while (blockMaterial == Material.WATER || blockMaterial == Material.LAVA);

        return new Location(world, x + 0.5, y, z + 0.5, yaw, pitch);
    }
}
