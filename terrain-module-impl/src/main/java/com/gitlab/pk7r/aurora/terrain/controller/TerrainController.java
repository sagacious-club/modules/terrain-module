package com.gitlab.pk7r.aurora.terrain.controller;

import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TerrainController {

    FileConfiguration fileConfiguration;

    @EventListener(ContextRefreshedEvent.class)
    public void createTerrainsWorld() {
        val worldName = fileConfiguration.getConfiguration("terrain.yml").getString("terrains-world");
        val worldBorder = fileConfiguration.getConfiguration("terrain.yml").getInt("terrains-world-border");
        if (Bukkit.getWorld(worldName != null ? worldName : "terrenos") == null) {
            val worldCreator = new WorldCreator(worldName != null ? worldName : "terrenos");
            worldCreator.environment(World.Environment.NORMAL);
            val world = worldCreator.createWorld();
            if (world != null) {
                world.setDifficulty(Difficulty.NORMAL);
                world.setAutoSave(true);
                world.setPVP(true);
                world.getWorldBorder().setSize(worldBorder);
            }
        } else {
            val world = Bukkit.getWorld(worldName != null ? worldName : "terrenos");
            if (world != null) {
                if (worldBorder < world.getWorldBorder().getSize()) return;
                world.getWorldBorder().setSize(worldBorder);
            }
        }
    }
}