package com.gitlab.pk7r.aurora.terrain.command.subcommands.admin;

import com.gitlab.pk7r.aurora.terrain.command.subcommands.admin.set.TerrainAdminSetCommand;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainFlagService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;

public class TerrainAdminCommand {

    public static CommandAPICommand terrainAdminCommand(TerrainService terrainService, TerrainPlayerService terrainPlayerService, TerrainFlagService terrainFlagService, ChunkSerializationService chunkSerializationService) {
        return new CommandAPICommand("admin")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.chain.command.terreno.admin")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&6Terreno Admin - Ajuda\n").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/terreno admin set").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/terreno admin delete <nick>").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/terreno admin switch <nick>").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/terreno admin expand <nick>").formatted());
                })
                .withSubcommand(TerrainAdminExpandCommand.getCommand(terrainService))
                .withSubcommand(TerrainAdminSwitchCommand.getCommand(terrainService, terrainPlayerService, chunkSerializationService))
                .withSubcommand(TerrainAdminSetCommand.terrainAdminSetCommand(terrainService, terrainFlagService, terrainPlayerService, chunkSerializationService))
                .withSubcommand(TerrainAdminDeleteCommand.getInvalidCommand())
                .withSubcommand(TerrainAdminSwitchCommand.getInvalidCommand())
                .withSubcommand(TerrainAdminExpandCommand.getInvalidCommand())
                .withSubcommand(TerrainAdminDeleteCommand.getCommand(terrainService));
    }
}
