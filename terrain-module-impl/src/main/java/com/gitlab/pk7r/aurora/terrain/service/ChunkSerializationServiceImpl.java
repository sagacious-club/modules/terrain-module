package com.gitlab.pk7r.aurora.terrain.service;

import com.gitlab.pk7r.aurora.terrain.util.ChunkUtil;
import lombok.val;
import org.bukkit.Chunk;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChunkSerializationServiceImpl implements ChunkSerializationService {

    @Override
    public String serializeChunk(Chunk chunk) {
        return String.format("%s:%s:%s", chunk.getWorld().getName().toLowerCase(), chunk.getX(), chunk.getZ());
    }

    @Override
    public String serializeChunks(Chunk... chunks) {
        return serializeChunks(Arrays.stream(chunks).toList());
    }

    @Override
    public String serializeChunks(List<Chunk> chunks) {
        return chunks.stream().map(this::serializeChunk).collect(Collectors.joining(";"));
    }

    @Override
    public Chunk deserializeChunk(String chunk) {
        val serializedChunk = chunk.split(":");
        return ChunkUtil.getChunk(serializedChunk[0], Integer.parseInt(serializedChunk[1]), Integer.parseInt(serializedChunk[2]));
    }

    @Override
    public List<Chunk> deserializeChunks(String chunks) {
        val serializedChunks = chunks.split(";");
        return Arrays.stream(serializedChunks).map(this::deserializeChunk).collect(Collectors.toList());
    }
}