package com.gitlab.pk7r.aurora.terrain.command.subcommands.admin.set;

import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class TerrainAdminSetOwnerCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainPlayerService terrainPlayerService, ChunkSerializationService chunkSerializationService) {
        return new CommandAPICommand("owner")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.set.owner")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    if (terrainService.hasTerrain(target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador já tem um terreno.").formatted());
                        return;
                    }
                    val chunk = chunkSerializationService.serializeChunk(player.getLocation().getChunk());
                    if (!terrainService.isTerrain(chunk, true)) {
                        player.spigot().sendMessage(new Message("&cVocê não está em um terreno.").formatted());
                        return;
                    }
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    if (terrainPlayerService.isOwner(terrain, target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador já é dono deste terreno.").formatted());
                        return;
                    }
                    val oldOwner = terrain.getOwner();
                    terrainService.setTerrainOwner(terrain, target.getName().toLowerCase());
                    player.spigot().sendMessage(new Message(String.format("&aVocê atualizou o dono do terreno de &e%s &apara &e%s&a.", oldOwner, terrain.getOwner())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("owner")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.set.owner")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno admin set owner <jogador>").formatted());
                });
    }
}
