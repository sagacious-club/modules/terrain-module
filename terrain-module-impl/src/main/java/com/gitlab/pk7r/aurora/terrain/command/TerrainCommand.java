package com.gitlab.pk7r.aurora.terrain.command;

import com.gitlab.pk7r.aurora.Command;
import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import com.gitlab.pk7r.aurora.service.DelayService;
import com.gitlab.pk7r.aurora.service.LocationSerializationService;
import com.gitlab.pk7r.aurora.terrain.command.subcommands.*;
import com.gitlab.pk7r.aurora.terrain.command.subcommands.admin.TerrainAdminCommand;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainFlagService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Command
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TerrainCommand {

    TerrainService terrainService;

    TerrainFlagService terrainFlagService;

    TerrainPlayerService terrainPlayerService;

    ChunkSerializationService chunkSerializationService;

    DelayService delayService;

    FileConfiguration fileConfiguration;

    LocationSerializationService locationSerializationService;

    @PostConstruct
    public void terrainCommand() {
        new CommandAPICommand("terreno")
                .executesPlayer((player, args) -> {
                    player.performCommand("terreno 1");
                })
                .withSubcommand(new CommandAPICommand("1")
                        .executesPlayer((player, objects) -> {
                            if (!player.hasPermission("aurora.terrain.command.terreno")) {
                                player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                                return;
                            }
                            player.spigot().sendMessage(new Message("&aTerreno - Ajuda\n").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terrenos").formatted());
                            if (player.hasPermission("aurora.chain.command.terreno.admin")) {
                                player.spigot().sendMessage(new Message("&6- &e/terreno admin").formatted());
                            }
                            player.spigot().sendMessage(new Message("&6- &e/terreno coletar").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno expandir").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno abandonar").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno info <jogador>").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno home <jogador>\n").formatted());
                            player.spigot().sendMessage(new Message("&7[&f1&7/&f2&7] &f[[»]](/terreno 2 hover=&6Próxima Página)").formatted());
                        }))
                .withSubcommand(new CommandAPICommand("2")
                        .executesPlayer((player, objects) -> {
                            if (!player.hasPermission("aurora.terrain.command.terreno")) {
                                player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                                return;
                            }
                            player.spigot().sendMessage(new Message("&aTerreno - Ajuda\n").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno borda").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno ban <jogador>").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno unban <jogador>").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno confiar <jogador> <PARCIAL/TOTAL>").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno desconfiar <jogador>").formatted());
                            player.spigot().sendMessage(new Message("&6- &e/terreno set <flag> <valor>\n").formatted());
                            player.spigot().sendMessage(new Message("&f[[«]](/terreno 1 hover=&6Página Anterior) " + "&7[&f2&7/&f2&7]").formatted());
                        }))
                .withSubcommand(TerrainAdminCommand.terrainAdminCommand(terrainService, terrainPlayerService, terrainFlagService, chunkSerializationService))
                .withSubcommand(TerrainClaimCommand.getCommand(terrainService, chunkSerializationService))
                .withSubcommand(TerrainAbandonCommand.getCommand(terrainService))
                .withSubcommand(TerrainHomeCommand.getCommand(terrainService, locationSerializationService)
                        .withSubcommand(new CommandAPICommand("set")
                                .executesPlayer((player, args) -> {
                                    if (!player.hasPermission("aurora.terrain.command.terreno.home.set")) {
                                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                                        return;
                                    }
                                    if (terrainService.inTerrainsWorld(player)) {
                                        player.spigot().sendMessage(new Message("&cVocê precisa estar no mundo dos terrenos para executar este comando.").formatted());
                                        return;
                                    }
                                    if (!terrainService.hasTerrain(player)) {
                                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                                        return;
                                    }
                                    val chunk = chunkSerializationService.serializeChunk(player.getLocation().getChunk());
                                    if (!terrainService.isTerrain(chunk, false)) {
                                        player.spigot().sendMessage(new Message("&cVocê não está em um terreno.").formatted());
                                        return;
                                    }
                                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                                    if (!terrainPlayerService.isOwner(terrain, player)) {
                                        player.spigot().sendMessage(new Message("&cVocê não está no seu terreno.").formatted());
                                        return;
                                    }
                                    terrainService.updateTerrainHome(terrain, player.getLocation());
                                    player.spigot().sendMessage(new Message("&aLocalização da home do seu terreno alterada com sucesso.").formatted());
                                })))
                .withSubcommand(TerrainHomeCommand.getCompleteCommand(terrainService, locationSerializationService, terrainPlayerService))
                .withSubcommand(TerrainInfoCommand.getCommand(terrainService, chunkSerializationService))
                .withSubcommand(TerrainBanCommand.getCommand(terrainService, terrainPlayerService))
                .withSubcommand(TerrainBanCommand.getInvalidCommand())
                .withSubcommand(TerrainUnbanCommand.getCommand(terrainService, terrainPlayerService))
                .withSubcommand(TerrainUnbanCommand.getInvalidCommand())
                .withSubcommand(TerrainUntrustCommand.getCommand(terrainService, terrainPlayerService))
                .withSubcommand(TerrainUntrustCommand.getInvalidCommand())
                .withSubcommand(TerrainBorderCommand.getCommand(terrainService, chunkSerializationService, delayService))
                .withSubcommand(TerrainSetCommand.getCommand(terrainService, terrainFlagService))
                .withSubcommand(TerrainSetCommand.getHelpCommand())
                .withSubcommand(TerrainSetCommand.getInvalidCommand())
                .withSubcommand(TerrainTrustCommand.getInvalidCommand(terrainService))
                .withSubcommand(TerrainTrustCommand.getInvalidCommand())
                .withSubcommand(TerrainTrustCommand.getCommand(terrainService, terrainPlayerService))
                .withSubcommand(TerrainExpandCommand.getCommand(terrainService, terrainPlayerService, chunkSerializationService, delayService))
                .register();
    }
}