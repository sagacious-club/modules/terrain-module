package com.gitlab.pk7r.aurora.terrain.command.subcommands.admin;

import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class TerrainAdminDeleteCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService) {
        return new CommandAPICommand("delete")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.delete")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    if (!terrainService.hasTerrain(target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador não tem um terreno.").formatted());
                        return;
                    }
                    terrainService.deleteTerrain(target);
                    player.spigot().sendMessage(new Message(String.format("&aVocê deletou o terreno de &e%s&a.", target.getName())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("delete")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.delete")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno admin delete <jogador>").formatted());
                });
    }
}
