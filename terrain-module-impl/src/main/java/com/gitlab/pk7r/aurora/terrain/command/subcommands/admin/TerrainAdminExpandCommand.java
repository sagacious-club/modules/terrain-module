package com.gitlab.pk7r.aurora.terrain.command.subcommands.admin;

import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class TerrainAdminExpandCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService) {
        return new CommandAPICommand("expand")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.expand")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    if (!terrainService.hasTerrain(target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador não tem um terreno.").formatted());
                        return;
                    }
                    terrainService.expandTerrain(target);
                    player.spigot().sendMessage(new Message(String.format("&aVocê expandiu o terreno de &e%s&a.", target.getName())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("expand")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.expand")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno admin expand <jogador>").formatted());
                });
    }
}
