package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class TerrainBanCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainPlayerService terrainPlayerService) {
        return new CommandAPICommand("ban")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.ban")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    val terrain = terrainService.getPlayerTerrain(player).orElseThrow();
                    if (terrainPlayerService.isOwner(terrain, target)) {
                        player.spigot().sendMessage(new Message("&cJogador inválido.").formatted());
                        return;
                    }
                    if (terrainPlayerService.isBanned(terrain, target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador já está banido no seu terreno.").formatted());
                        return;
                    }
                    terrainPlayerService.setPlayerTrustState(terrain.getId(), target, TrustState.BANNED);
                    player.spigot().sendMessage(new Message(String.format("&aVocê baniu &e%s &ado seu terreno.", target.getName())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("ban")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.ban")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno ban <jogador>").formatted());
                });
    }
}