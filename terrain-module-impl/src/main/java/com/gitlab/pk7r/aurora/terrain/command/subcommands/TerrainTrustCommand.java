package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.PlayerArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class TerrainTrustCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainPlayerService terrainPlayerService) {
        return new CommandAPICommand("confiar")
                .withArguments(new PlayerArgument("jogador"), new StringArgument("tipo").replaceSuggestions(ArgumentSuggestions.strings("PARCIAL", "TOTAL")))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.confiar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    val value = (String) args[1];
                    val terrain = terrainService.getPlayerTerrain(player).orElseThrow();
                    if (terrainPlayerService.isOwner(terrain, target)) {
                        player.spigot().sendMessage(new Message("&cJogador inválido.").formatted());
                        return;
                    }
                    if (terrainPlayerService.isBanned(terrain, target)) {
                        player.spigot().sendMessage(new Message("&cEste jogador está banido do seu terreno.").formatted());
                        return;
                    }
                    if (!value.equalsIgnoreCase("PARCIAL") && !value.equalsIgnoreCase("TOTAL")) {
                        player.spigot().sendMessage(new Message("&cTipo de confiança inválida.").formatted());
                        return;
                    }
                    if (terrainPlayerService.isMember(terrain, target) && value.equalsIgnoreCase("PARCIAL")) {
                        player.spigot().sendMessage(new Message("&cEste jogador já é confiado parcialmente no seu terreno.").formatted());
                        return;
                    }
                    if (terrainPlayerService.isTrusted(terrain, target) && value.equalsIgnoreCase("TOTAL")) {
                        player.spigot().sendMessage(new Message("&cEste jogador já é confiado totalmente no seu terreno.").formatted());
                        return;
                    }
                    terrainPlayerService.setPlayerTrustState(terrain.getId(), target, value.equalsIgnoreCase("PARCIAL") ? TrustState.PARTIAL : TrustState.TOTAL);
                    player.spigot().sendMessage(new Message(String.format("&aVocê confiou %s &e%s &ano seu terreno.",
                            value.equalsIgnoreCase("PARCIAL") ? "parcialmente" : "totalmente",  target.getName())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("confiar")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.confiar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno confiar <jogador> <tipo>").formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand(TerrainService terrainService) {
        return new CommandAPICommand("confiar")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.confiar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno confiar <jogador> <tipo>").formatted());
                });
    }
}
