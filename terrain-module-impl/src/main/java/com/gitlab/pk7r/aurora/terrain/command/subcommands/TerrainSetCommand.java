package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.terrain.model.Flag;
import com.gitlab.pk7r.aurora.terrain.model.FlagType;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainFlagService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import lombok.val;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TerrainSetCommand {

    public static CommandAPICommand getHelpCommand() {
        return new CommandAPICommand("set")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.set")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&aTerreno - Ajuda\n").formatted());
                    player.spigot().sendMessage(new Message("&aFlags disponíveis:").formatted());
                    val flags = Arrays.stream(Flag.values())
                            .map(flag -> String.format("[%s](suggest_command=%s hover=&6%s)", flag.name(), "/terreno set " + flag.name(), flag.getDescription().substring(2).substring(0, 1).toUpperCase() +
                                    flag.getDescription().substring(2).substring(1).toLowerCase()))
                            .collect(Collectors.collectingAndThen(Collectors.joining("&6, &e"),
                                    result -> result.isEmpty() ? "&cNenhuma flag disponível." : result));
                    player.spigot().sendMessage(new Message("&e" + flags).formatted());
                });
    }

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainFlagService terrainFlagService) {
        return new CommandAPICommand("set")
                .withArguments(new StringArgument("flag").replaceSuggestions(ArgumentSuggestions.strings(Arrays.stream(Flag.values()).map(Flag::name).toArray(String[]::new))),
                        new GreedyStringArgument("valor").replaceSuggestions(ArgumentSuggestions.strings("TRUE", "FALSE")))
                .executesPlayer((player, args) -> {
                    val flagName = (String) args[0];
                    val flagExists = Arrays.stream(Flag.values()).anyMatch(flag -> flag.name().equalsIgnoreCase(flagName));
                    if (!flagExists) {
                        player.spigot().sendMessage(new Message("&cFlag inválida.").formatted());
                        return;
                    }
                    val flag = Flag.valueOf(flagName);
                    if (!player.hasPermission("aurora.terrain.command.terreno.set" + flag.name())) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val value = (String) args[1];
                    if (flag.getFlagType().equals(FlagType.BOOLEAN)) {
                        if (!value.equalsIgnoreCase(Boolean.TRUE.toString()) && !value.equalsIgnoreCase(Boolean.FALSE.toString())) {
                            player.spigot().sendMessage(new Message("&cValor inválido.").formatted());
                            return;
                        }
                    }
                    if (!terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                        return;
                    }
                    val terrain = terrainService.getPlayerTerrain(player).orElseThrow();
                    terrainFlagService.updateTerrainFlag(terrain, flag, value);
                    if (flag.getFlagType().equals(FlagType.BOOLEAN)) {
                        player.spigot().sendMessage(new Message(String.format("&aVocê atualizou %s &apara &f%s&a.",
                                flag.getDescription(), value)).formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message(String.format("&aVocê atualizou %s&a.", flag.getDescription())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("set")
                .withArguments(new StringArgument("flag").replaceSuggestions(ArgumentSuggestions.strings(Arrays.stream(Flag.values()).map(Flag::name).toArray(String[]::new))))
                .executesPlayer((player, args) -> {
                    val flagName = (String) args[0];
                    val flagExists = Arrays.stream(Flag.values()).anyMatch(flag -> flag.name().equalsIgnoreCase(flagName));
                    if (!flagExists) {
                        player.spigot().sendMessage(new Message("&cFlag inválida.").formatted());
                        return;
                    }
                    val flag = Flag.valueOf(flagName);
                    if (!player.hasPermission("aurora.terrain.command.terreno.set." + flag.name())) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message(String.format("&cUso correto: /terreno set %s <valor>", flag.name())).formatted());
                });
    }

}
