package com.gitlab.pk7r.aurora.terrain.command.subcommands.admin.set;

import com.gitlab.pk7r.aurora.terrain.model.Flag;
import com.gitlab.pk7r.aurora.terrain.model.FlagType;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainFlagService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import lombok.val;

import java.util.Arrays;

public class TerrainAdminSetFlagCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainFlagService terrainFlagService, ChunkSerializationService chunkSerializationService) {
        return new CommandAPICommand("flag")
                .withArguments(new StringArgument("flag").replaceSuggestions(ArgumentSuggestions.strings(info -> Arrays.stream(Flag.values()).map(Flag::name).toArray(String[]::new))),
                        new GreedyStringArgument("valor").replaceSuggestions(ArgumentSuggestions.strings("TRUE", "FALSE")))
                .executesPlayer((player, args) -> {
                    val flagName = (String) args[0];
                    val flagExists = Arrays.stream(Flag.values()).anyMatch(flag -> flag.name().equalsIgnoreCase(flagName));
                    if (!flagExists) {
                        player.spigot().sendMessage(new Message("&cFlag inválida.").formatted());
                        return;
                    }
                    val flag = Flag.valueOf(flagName);
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.set.flag." + flag.name())) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val value = (String) args[1];
                    if (flag.getFlagType().equals(FlagType.BOOLEAN)) {
                        if (!value.equalsIgnoreCase(Boolean.TRUE.toString()) && !value.equalsIgnoreCase(Boolean.FALSE.toString())) {
                            player.spigot().sendMessage(new Message("&cValor inválido.").formatted());
                            return;
                        }
                    }
                    if (terrainService.inTerrainsWorld(player)) {
                        return;
                    }
                    val chunk = chunkSerializationService.serializeChunk(player.getLocation().getChunk());
                    if (terrainService.isTerrain(chunk, true)) {
                        player.spigot().sendMessage(new Message("&cVocê não está em um terreno.").formatted());
                        return;
                    }
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    terrainFlagService.updateTerrainFlag(terrain, flag, value);
                    if (flag.getFlagType().equals(FlagType.BOOLEAN)) {
                        player.spigot().sendMessage(new Message(String.format("&aVocê atualizou %s &apara &f%s &ano terreno de &e%s&a.",
                                flag.getDescription(), value, terrain.getOwner())).formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message(String.format("&aVocê atualizou %s &ano terreno de &e%s&a.", flag.getDescription(), terrain.getOwner())).formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("flag")
                .withArguments(new StringArgument("flag").replaceSuggestions(ArgumentSuggestions.strings(info -> Arrays.stream(Flag.values()).map(Flag::name).toArray(String[]::new))))
                .executesPlayer((player, args) -> {
                    val flagName = (String) args[0];
                    val flagExists = Arrays.stream(Flag.values()).anyMatch(flag -> flag.name().equalsIgnoreCase(flagName));
                    if (!flagExists) {
                        player.spigot().sendMessage(new Message("&cFlag inválida.").formatted());
                        return;
                    }
                    val flag = Flag.valueOf(flagName);
                    if (!player.hasPermission("aurora.terrain.command.terreno.admin.set.flag." + flag.name())) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message(String.format("&cUso correto: /terreno admin set flag %s <valor>", flag.name())).formatted());
                });
    }
}
