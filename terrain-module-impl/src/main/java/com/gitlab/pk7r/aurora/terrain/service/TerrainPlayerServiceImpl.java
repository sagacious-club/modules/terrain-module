package com.gitlab.pk7r.aurora.terrain.service;

import com.gitlab.pk7r.aurora.terrain.event.TerrainPlayerUpdateTrustStateEvent;
import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.model.TerrainPlayer;
import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import com.gitlab.pk7r.aurora.terrain.repository.TerrainPlayerRepository;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TerrainPlayerServiceImpl implements TerrainPlayerService {

    Plugin plugin;

    TerrainService terrainService;

    TerrainPlayerRepository terrainPlayerRepository;

    @Override
    public void setPlayerTrustState(Integer terrainId, Player player, TrustState trustState) {
        if (terrainService.getTerrain(terrainId).isEmpty()) return;
        val terrain = terrainService.getTerrain(terrainId).get();
        val terrainPlayer = getTerrainPlayer(terrain, player).orElse(new TerrainPlayer());
        terrainPlayer.setTerrain(terrain);
        terrainPlayer.setUsername(player.getName().toLowerCase());
        val event = new TerrainPlayerUpdateTrustStateEvent(terrain, player, terrainPlayer.getTrustState(), trustState);
        terrainPlayer.setTrustState(trustState);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) terrainPlayerRepository.save(terrainPlayer);
    }

    @Override
    public List<TerrainPlayer> getTerrainPlayers(Terrain terrain) {
        return terrainPlayerRepository.findByTerrain(terrain);
    }

    @Override
    public Optional<TerrainPlayer> getTerrainPlayer(Terrain terrain, Player player) {
        return getTerrainPlayers(terrain).stream().filter(terrainPlayer -> terrainPlayer.getUsername().equalsIgnoreCase(player.getName())).findFirst();
    }

    @Override
    public boolean isMember(Terrain terrain, Player player) {
        val optionalTerrainPlayer = getTerrainPlayer(terrain, player);
        if (optionalTerrainPlayer.isEmpty()) return false;
        val terrainPlayer = optionalTerrainPlayer.get();
        return terrainPlayer.getTrustState().equals(TrustState.PARTIAL);
    }

    @Override
    public boolean isTrusted(Terrain terrain, Player player) {
        val optionalTerrainPlayer = getTerrainPlayer(terrain, player);
        if (optionalTerrainPlayer.isEmpty()) return false;
        val terrainPlayer = optionalTerrainPlayer.get();
        return terrainPlayer.getTrustState().equals(TrustState.TOTAL);
    }

    @Override
    public boolean isBanned(Terrain terrain, Player player) {
        val optionalTerrainPlayer = getTerrainPlayer(terrain, player);
        if (optionalTerrainPlayer.isEmpty()) return false;
        val terrainPlayer = optionalTerrainPlayer.get();
        return terrainPlayer.getTrustState().equals(TrustState.BANNED);
    }

    @Override
    public boolean isOwner(Terrain terrain, Player player) {
        val optionalTerrain = terrainService.getPlayerTerrain(player);
        if (optionalTerrain.isEmpty()) return false;
        val targetTerrain = optionalTerrain.get();
        return terrain.getOwner().equalsIgnoreCase(targetTerrain.getOwner());
    }
}