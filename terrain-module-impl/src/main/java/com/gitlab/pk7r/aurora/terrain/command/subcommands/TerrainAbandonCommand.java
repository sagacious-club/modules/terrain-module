package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.Command;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;

@Command
public class TerrainAbandonCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService) {
        return new CommandAPICommand("abandonar")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.abandonar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                        return;
                    }
                    terrainService.deleteTerrain(player);
                    player.spigot().sendMessage(new Message("&aVocê abandonou o seu terreno com sucesso.").formatted());
                });
    }
}