package com.gitlab.pk7r.aurora.terrain.service;

import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import com.gitlab.pk7r.aurora.service.LocationSerializationService;
import com.gitlab.pk7r.aurora.service.SchedulerService;
import com.gitlab.pk7r.aurora.terrain.event.TerrainAbandonEvent;
import com.gitlab.pk7r.aurora.terrain.event.TerrainClaimEvent;
import com.gitlab.pk7r.aurora.terrain.event.TerrainExpandEvent;
import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.repository.TerrainRepository;
import com.gitlab.pk7r.aurora.terrain.util.ChunkUtil;
import com.gitlab.pk7r.aurora.util.Message;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TerrainServiceImpl implements TerrainService {

    private final ArrayList<Pair<String, Boolean>> loadedChunks = new ArrayList<>();

    Plugin plugin;

    SchedulerService schedulerService;

    TerrainFlagService terrainFlagService;

    TerrainRepository terrainRepository;

    FileConfiguration fileConfiguration;

    LocationSerializationService locationSerializationService;

    ChunkSerializationService chunkSerializationService;

    @Scheduled(cron = "0 0/30 * * * *")
    public void clearCache() {
        loadedChunks.stream().filter(Objects::nonNull).filter(chunk -> !chunk.getSecond()).forEach(loadedChunks::remove);
    }

    @Override
    public void createTerrain(Player player, String owner, Location location) {
        val centerChunk = location.getChunk();
        val centerLocation = ChunkUtil.getCenterChunkLocation(centerChunk);
        val terrain = new Terrain();
        val terrainChunks = ChunkUtil.around(centerChunk, 1);
        terrainChunks.add(centerChunk);
        terrain.setChunks(chunkSerializationService.serializeChunks(terrainChunks.toArray(Chunk[]::new)));
        terrain.setCenter(locationSerializationService.serializeLocation(centerLocation));
        terrain.setHome(terrain.getCenter());
        terrain.setExpansions(1);
        terrain.setOwner(owner.toLowerCase());
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            val event = new TerrainClaimEvent(true, terrain, player);
            Bukkit.getPluginManager().callEvent(event);
            if (!event.isCancelled()) {
                chunkSerializationService.deserializeChunks(terrain.getChunks())
                        .stream()
                        .map(chunk -> chunkSerializationService.serializeChunk(chunk))
                        .filter(chunk -> loadedChunks.stream().anyMatch(loadedChunk -> chunk.equalsIgnoreCase(loadedChunk.getFirst())))
                        .forEach(chunk -> {
                            loadedChunks.remove(Pair.of(chunk, true));
                            loadedChunks.remove(Pair.of(chunk, false));
                        });
                terrainRepository.save(terrain);
                terrainFlagService.setDefaultFlags(terrain);
                Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> showBorder(player, terrain, Particle.DRIP_LAVA, 15), 100L);
            }
        });
    }

    @Override
    public void expandTerrain(Player player) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> getPlayerTerrain(player).ifPresent(terrain -> {
            val event = new TerrainExpandEvent(true, terrain, player);
            Bukkit.getPluginManager().callEvent(event);
            if (!event.isCancelled()) {
                val expansionsLimit = getExpansionsLimit(player);
                if (player.hasPermission("aurora.terrain.bypass.expandlimit") || expansionsLimit >= terrain.getExpansions() + 1) {
                    val centerLocation = locationSerializationService.deserializeLocation(terrain.getCenter());
                    val centerChunk = centerLocation.getChunk();
                    val terrainChunks = ChunkUtil.around(centerChunk, terrain.getExpansions() + 2);
                    terrainChunks.add(centerChunk);
                    if (hasTerrainAround(chunkSerializationService.serializeChunks(terrainChunks.toArray(Chunk[]::new)), terrain.getOwner())) {
                        player.spigot().sendMessage(new Message("&cFalha ao expandir, o seu terreno está muito próximo outro.").formatted());
                        event.setCancelled(true);
                        return;
                    }
                    terrain.setChunks(chunkSerializationService.serializeChunks(ChunkUtil.around(centerChunk, terrain.getExpansions() + 1).toArray(Chunk[]::new)));
                    terrain.setExpansions(terrain.getExpansions() + 1);
                    chunkSerializationService.deserializeChunks(terrain.getChunks())
                            .stream()
                            .map(chunk -> chunkSerializationService.serializeChunk(chunk))
                            .filter(chunk -> loadedChunks.stream().anyMatch(loadedChunk -> chunk.equalsIgnoreCase(loadedChunk.getFirst())))
                            .forEach(chunk -> {
                                loadedChunks.remove(Pair.of(chunk, true));
                                loadedChunks.remove(Pair.of(chunk, false));
                            });
                    terrainRepository.save(terrain);
                    player.spigot().sendMessage(new Message("&aVocê expandiu o seu terreno com sucesso.").formatted());
                } else {
                    player.spigot().sendMessage(new Message("&cVocê não pode expandir o seu terreno.").formatted());
                    event.setCancelled(true);
                }
            }
        }));
    }

    @Override
    public void updateTerrainHome(Terrain terrain, Location location) {
        terrain.setHome(locationSerializationService.serializeLocation(location));
        terrainRepository.save(terrain);
    }

    @Override
    public List<Terrain> getTerrains() {
        return terrainRepository.findAll();
    }

    @Override
    public Optional<Terrain> getTerrain(Integer id) {
        return terrainRepository.findById(id);
    }

    @Override
    public Optional<Terrain> getPlayerTerrain(Player player) {
        return terrainRepository.findByOwner(player.getName().toLowerCase());
    }

    @Override
    public Optional<Terrain> getPlayerTerrain(String player) {
        return terrainRepository.findByOwner(player.toLowerCase());
    }

    @Override
    public Optional<Terrain> getTerrainByChunk(String chunk) {
        return terrainRepository.findTerrainByChunksContaining(chunk);
    }

    @Override
    public boolean isTerrain(String chunk, boolean cache) {
        if (!cache) {
            val isLoaded = loadedChunks.stream().anyMatch(c -> c.getFirst().equalsIgnoreCase(chunk));
            if (!isLoaded) {
                loadedChunks.add(Pair.of(chunk, terrainRepository.existsByChunksContaining(chunk)));
            }
            return loadedChunks.stream().filter(c -> c.getFirst().equalsIgnoreCase(chunk)).anyMatch(Pair::getSecond);
        }
        return terrainRepository.existsByChunksContaining(chunk);
    }

    @Override
    public boolean hasTerrain(Player player) {
        return terrainRepository.existsByOwner(player.getName().toLowerCase());
    }

    @Override
    public boolean hasTerrain(String player) {
        return terrainRepository.existsByOwner(player.toLowerCase());
    }

    @Override
    public boolean inTerrainsWorld(Player player) {
        return !player.getWorld().getName().equalsIgnoreCase(fileConfiguration.getConfiguration("terrain.yml").getString("terrains-world"));
    }

    @Override
    public void showBorder(Player player, Terrain terrain, Particle particle, int timeInSeconds) {
        chunkSerializationService.deserializeChunks(terrain.getChunks()).forEach(chunk -> {
            val world = chunk.getWorld();
            val chunkX = chunk.getX();
            val chunkZ = chunk.getZ();
            val minX = chunkX * 16;
            val minZ = chunkZ * 16;

            Bukkit.getScheduler().runTaskAsynchronously(plugin, () ->
                    new BukkitRunnable() {
                private int i = timeInSeconds; public void run() {
                    if (getTerrain(terrain.getId()).isEmpty()) cancel();
                    if (i == 0) {
                        cancel();
                    }
                    --i;
                    val north = world.getChunkAt(chunkX, chunkZ - 1);
                    if (!isTerrain(chunkSerializationService.serializeChunk(north), true)) {
                        for (int x = minX; x < minX + 16; x++) {
                            val y = player.getLocation().getBlockY() + 1;
                            val block = north.getWorld().getBlockAt(x, y, minZ);
                            if (block.getType().isAir()) {
                                player.spawnParticle(particle, x + 0.5, y, minZ + 0.5, 3);
                            }
                            if (north.getWorld().getBlockAt(x, y + 1, minZ).getType().isAir()) {
                                player.spawnParticle(particle, x + 0.5, y + 1, minZ + 0.5, 1);
                            }
                            if (north.getWorld().getBlockAt(x, y + 2, minZ).getType().isAir()) {
                                player.spawnParticle(particle, x + 0.5, y + 2, minZ + 0.5, 3);
                            }
                        }
                    }

                    val south = world.getChunkAt(chunkX, chunkZ + 1);
                    if (!isTerrain(chunkSerializationService.serializeChunk(south), true)) {
                        for (int x = minX; x < minX + 16; x++) {
                            val y = player.getLocation().getBlockY() + 1;
                            val block = south.getWorld().getBlockAt(x, y, minZ + 16);
                            if (block.getType().isAir()) {
                                player.spawnParticle(particle, x - 0.5, y, minZ - 0.5 + 16, 3);
                            }
                            if (south.getWorld().getBlockAt(x, y + 1, minZ + 16).getType().isAir()) {
                                player.spawnParticle(particle, x - 0.5, y + 1, minZ - 0.5 + 16, 3);
                            }
                            if (south.getWorld().getBlockAt(x, y + 2, minZ + 16).getType().isAir()) {
                                player.spawnParticle(particle, x - 0.5, y + 2, minZ - 0.5 + 16, 3);
                            }
                        }
                    }

                    val west = world.getChunkAt(chunkX - 1, chunkZ);
                    if (!isTerrain(chunkSerializationService.serializeChunk(west), true)) {
                        for (int z = minZ; z < minZ + 16; z++) {
                            val y = player.getLocation().getBlockY() + 1;
                            val block = west.getWorld().getBlockAt(minX, y, z);
                            if (block.getType().isAir()) {
                                player.spawnParticle(particle, minX + 0.5, y, z + 0.5, 3);
                            }
                            if (west.getWorld().getBlockAt(minX, y + 1, z).getType().isAir()) {
                                player.spawnParticle(particle, minX + 0.5, y + 1, z + 0.5, 3);
                            }
                            if (west.getWorld().getBlockAt(minX, y + 2, z).getType().isAir()) {
                                player.spawnParticle(particle, minX + 0.5, y + 2, z + 0.5, 3);
                            }
                        }
                    }

                    val east = world.getChunkAt(chunkX + 1, chunkZ);
                    if (!isTerrain(chunkSerializationService.serializeChunk(east), true)) {
                        for (int z = minZ; z < minZ + 16; z++) {
                            val y = player.getLocation().getBlockY() + 1;
                            val block = east.getWorld().getBlockAt(minX + 16, y, z);
                            if (block.getType().isAir()) {
                                player.spawnParticle(particle, minX - 0.5 + 16, y, z - 0.5, 3);
                            }
                            if (east.getWorld().getBlockAt(minX + 16, y + 1, z).getType().isAir()) {
                                player.spawnParticle(particle, minX - 0.5 + 16, y + 1, z - 0.5, 3);
                            }
                            if (east.getWorld().getBlockAt(minX + 16, y + 2, z).getType().isAir()) {
                                player.spawnParticle(particle, minX - 0.5 + 16, y + 2, z - 0.5, 3);
                            }
                        }
                    }
                }
            }.runTaskTimerAsynchronously(plugin, 20L, 20L));
        });
    }

    @Override
    public boolean hasTerrainAround(String chunks) {
        return chunkSerializationService.deserializeChunks(chunks)
                .stream()
                .map(c -> chunkSerializationService.serializeChunk(c))
                .anyMatch(c -> terrainRepository.existsByChunksContaining(c));
    }

    @Override
    public boolean hasTerrainAround(String chunks, String owner) {
        return chunkSerializationService.deserializeChunks(chunks)
                .stream()
                .map(c -> chunkSerializationService.serializeChunk(c))
                .anyMatch(c -> terrainRepository.existsByChunksContainingAndOwnerIsNot(c, owner));
    }

    @Override
    public void deleteTerrain(Player player) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> getPlayerTerrain(player).ifPresent(terrain -> {
            val event = new TerrainAbandonEvent(true, terrain, player);
            Bukkit.getPluginManager().callEvent(event);
            if (!event.isCancelled()) {
                chunkSerializationService.deserializeChunks(terrain.getChunks())
                        .stream()
                        .map(chunk -> chunkSerializationService.serializeChunk(chunk))
                        .filter(chunk -> loadedChunks.stream().anyMatch(loadedChunk -> chunk.equalsIgnoreCase(loadedChunk.getFirst())))
                        .forEach(chunk -> {
                            loadedChunks.remove(Pair.of(chunk, true));
                            loadedChunks.remove(Pair.of(chunk, false));
                        });
                terrainRepository.delete(terrain);
            }
        }));
    }

    @Override
    public void setTerrainOwner(Terrain terrain, String newOwner) {
        terrain.setOwner(newOwner.toLowerCase());
        terrainRepository.save(terrain);
    }

    private int getExpansionsLimit(Player player) {
        return player.getEffectivePermissions()
                .stream()
                .filter(permissionAttachmentInfo -> permissionAttachmentInfo.getPermission().contains("aurora.terrain.expansions."))
                .findFirst()
                .map(permissionAttachmentInfo -> {
                    val list = Arrays.stream(permissionAttachmentInfo.getPermission().split("\\.")).toList();
                    return Integer.parseInt(list.get(list.size() - 1)) + 2;
                }).orElse(0);
    }
}