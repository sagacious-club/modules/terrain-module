package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.terrain.model.TerrainPlayer;
import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class TerrainUntrustCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, TerrainPlayerService terrainPlayerService) {
        return new CommandAPICommand("desconfiar")
                .withArguments(new PlayerArgument("jogador").replaceSuggestions(ArgumentSuggestions.strings(info -> {
                    if (terrainService.getPlayerTerrain(info.sender().getName()).isEmpty()) return new String[]{};
                    return terrainService.getPlayerTerrain(info.sender().getName()).get()
                            .getPlayers().stream()
                            .filter(terrainPlayer -> !terrainPlayer.getTrustState().equals(TrustState.UNTRUSTED) &&
                                    !terrainPlayer.getTrustState().equals(TrustState.BANNED))
                            .map(TerrainPlayer::getUsername).toArray(String[]::new);
                })))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.desconfiar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!terrainService.hasTerrain(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não tem um terreno.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    val terrain = terrainService.getPlayerTerrain(player).orElseThrow();
                    if (terrainPlayerService.isOwner(terrain, target)) {
                        player.spigot().sendMessage(new Message("&cJogador inválido.").formatted());
                        return;
                    }
                    if (terrainPlayerService.isMember(terrain, target) || terrainPlayerService.isTrusted(terrain, target)) {
                        terrainPlayerService.setPlayerTrustState(terrain.getId(), target, TrustState.UNTRUSTED);
                        player.spigot().sendMessage(new Message(String.format("&aVocê removeu a confiança de &e%s &ano seu terreno.", target.getName())).formatted());
                    } else player.spigot().sendMessage(new Message("&cEste jogador não está confiado no seu terreno.").formatted());
                });
    }

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("desconfiar")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.desconfiar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto /terreno desconfiar <jogador>").formatted());
                });
    }
}
