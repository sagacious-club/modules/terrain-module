package com.gitlab.pk7r.aurora.terrain.command;

import com.gitlab.pk7r.aurora.Command;
import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import com.gitlab.pk7r.aurora.service.DelayService;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.terrain.util.ChunkUtil;
import com.gitlab.pk7r.aurora.terrain.util.LocationUtil;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Command
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TerrainsCommand {

    DelayService delayService;

    FileConfiguration fileConfiguration;

    TerrainService terrainService;

    ChunkSerializationService chunkSerializationService;

    @PostConstruct
    public void terrainsCommand() {
        new CommandAPICommand("terrenos")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terrenos")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!delayService.assertDelay(player, player.getName() + ":terrains", 180, TimeUnit.SECONDS)) {
                        Location location = LocationUtil.genRandomLocation(Objects.requireNonNull(Bukkit.getWorld(Objects.requireNonNull(fileConfiguration.getConfiguration("terrain.yml").getString("terrains-world")))), player.getLocation().getYaw(), player.getLocation().getPitch());
                        Chunk centerChunk = location.getChunk();
                        Collection<Chunk> terrainChunks = ChunkUtil.around(centerChunk, 2);
                        terrainChunks.add(centerChunk);
                        while (terrainService.hasTerrainAround(chunkSerializationService.serializeChunks(terrainChunks.toArray(Chunk[]::new)))) {
                            location = LocationUtil.genRandomLocation(Objects.requireNonNull(Bukkit.getWorld(Objects.requireNonNull(fileConfiguration.getConfiguration("terrain.yml").getString("terrains-world")))), player.getLocation().getYaw(), player.getLocation().getPitch());
                            centerChunk = location.getChunk();
                            terrainChunks = ChunkUtil.around(centerChunk, 2);
                            terrainChunks.add(centerChunk);
                        }
                        player.teleport(location);
                        player.spigot().sendMessage(new Message("&aTeletransportado para o mundo dos terrenos.").formatted());
                    }
                }).register();
    }
}
