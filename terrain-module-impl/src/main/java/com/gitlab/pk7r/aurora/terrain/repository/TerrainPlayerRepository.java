package com.gitlab.pk7r.aurora.terrain.repository;

import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.model.TerrainPlayer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TerrainPlayerRepository extends JpaRepository<TerrainPlayer, Integer> {

    List<TerrainPlayer> findByTerrain(Terrain terrain);

}
