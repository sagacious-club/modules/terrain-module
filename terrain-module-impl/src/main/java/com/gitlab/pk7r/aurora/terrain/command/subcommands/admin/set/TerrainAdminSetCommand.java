package com.gitlab.pk7r.aurora.terrain.command.subcommands.admin.set;

import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainFlagService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainPlayerService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;

public class TerrainAdminSetCommand {

    public static CommandAPICommand terrainAdminSetCommand(TerrainService terrainService, TerrainFlagService terrainFlagService, TerrainPlayerService terrainPlayerService, ChunkSerializationService chunkSerializationService) {
        return new CommandAPICommand("set")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.chain.command.terreno.admin.set")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&6Terreno Set - Ajuda\n").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/terreno admin set flag <flag> <value>").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/terreno admin set owner <newOwner>").formatted());
                })
                .withSubcommand(TerrainAdminSetOwnerCommand.getCommand(terrainService, terrainPlayerService, chunkSerializationService))
                .withSubcommand(TerrainAdminSetFlagCommand.getCommand(terrainService, terrainFlagService, chunkSerializationService))
                .withSubcommand(TerrainAdminSetOwnerCommand.getInvalidCommand())
                .withSubcommand(TerrainAdminSetFlagCommand.getInvalidCommand());
    }
}
