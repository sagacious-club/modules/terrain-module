package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.terrain.model.FlagType;
import com.gitlab.pk7r.aurora.terrain.model.TerrainPlayer;
import com.gitlab.pk7r.aurora.terrain.model.TrustState;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.val;

import java.util.stream.Collectors;

public class TerrainInfoCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, ChunkSerializationService chunkSerializationService) {
        return new CommandAPICommand("info")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.info")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (terrainService.inTerrainsWorld(player)) {
                        player.spigot().sendMessage(new Message("&cVocê precisa estar no mundo dos terrenos para executar este comando.").formatted());
                        return;
                    }
                    val chunk = chunkSerializationService.serializeChunk(player.getLocation().getChunk());
                    if (!terrainService.isTerrain(chunk, false)) {
                        player.spigot().sendMessage(new Message("&cVocê não está em um terreno.").formatted());
                        return;
                    }
                    val terrain = terrainService.getTerrainByChunk(chunk).orElseThrow();
                    val terrainFlags = terrain.getFlags();
                    val banned = terrain.getPlayers().stream()
                            .filter(terrainPlayer -> terrainPlayer.getTrustState().equals(TrustState.BANNED))
                            .map(TerrainPlayer::getUsername)
                            .collect(Collectors.collectingAndThen(Collectors.joining("&6, &e"), result -> result.isEmpty() ? "&cNenhum jogador banido." : result));
                    val trusted = terrain.getPlayers().stream()
                            .filter(terrainPlayer -> terrainPlayer.getTrustState().equals(TrustState.TOTAL) || terrainPlayer.getTrustState().equals(TrustState.PARTIAL))
                            .map(terrainPlayer -> String.format("[%s](hover=&7Tipo de Confiança: &6%s)", terrainPlayer.getUsername(), terrainPlayer.getTrustState()))
                            .collect(Collectors.collectingAndThen(Collectors.joining("&6, &e"), result -> result.isEmpty() ? "&cNenhum jogador confiado." : result));
                    player.spigot().sendMessage(new Message(String.format("&aTerreno » &7%s\n", terrain.getOwner())).formatted());
                    terrainFlags.forEach(terrainFlag -> {
                        String formattedValue;
                        if (terrainFlag.getFlag().getFlagType().equals(FlagType.BOOLEAN)) {
                            formattedValue = terrainFlag.getValue().equalsIgnoreCase("true") ? "Sim" : "Não";
                        } else {
                            formattedValue = terrainFlag.getValue().equalsIgnoreCase("") ? "Nenhuma" :
                                    terrainFlag.getValue().contains(";") ?
                                            String.format("[[Ver]](hover=Título: &f%s &rSub-título: &f%s)",
                                                    terrainFlag.getValue().split(";")[0], terrainFlag.getValue().split(";")[1]) :
                                            String.format("[[Ver]](hover=Título: &f%s)", terrainFlag.getValue().split(";")[0]) ;
                        }
                        player.spigot().sendMessage(new Message(String.format("&6%s: &e%s",
                                terrainFlag.getFlag().getDescription().substring(2).substring(0, 1).toUpperCase() +
                                terrainFlag.getFlag().getDescription().substring(2).substring(1).toLowerCase(),
                                formattedValue)).formatted());
                    });
                    player.spigot().sendMessage(new Message(" ").formatted());
                    player.spigot().sendMessage(new Message(String.format("&6Banidos: &e%s", banned)).formatted());
                    player.spigot().sendMessage(new Message(String.format("&6Confiados: &e%s", trusted)).formatted());
                });
    }
}
