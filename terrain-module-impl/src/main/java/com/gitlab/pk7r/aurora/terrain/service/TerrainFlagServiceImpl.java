package com.gitlab.pk7r.aurora.terrain.service;

import com.gitlab.pk7r.aurora.terrain.event.TerrainUpdateFlagEvent;
import com.gitlab.pk7r.aurora.terrain.model.Flag;
import com.gitlab.pk7r.aurora.terrain.model.FlagType;
import com.gitlab.pk7r.aurora.terrain.model.Terrain;
import com.gitlab.pk7r.aurora.terrain.model.TerrainFlag;
import com.gitlab.pk7r.aurora.terrain.repository.TerrainFlagRepository;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("OptionalGetWithoutIsPresent")
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TerrainFlagServiceImpl implements TerrainFlagService {

    Plugin plugin;

    TerrainFlagRepository terrainFlagRepository;

    @Override
    public void setDefaultFlags(Terrain terrain) {
        Arrays.stream(Flag.values()).forEach(flag -> {
            val terrainFlag = new TerrainFlag();
            terrainFlag.setTerrain(terrain);
            terrainFlag.setFlag(flag);
            terrainFlag.setValue(flag.getFlagType().equals(FlagType.STRING) ? "" : flag.equals(Flag.ENTRY) ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
            terrainFlagRepository.save(terrainFlag);
        });
    }

    @Override
    public void updateTerrainFlag(Terrain terrain, Flag flag, String value) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            getTerrainFlags(terrain)
                    .stream()
                    .filter(terrainFlag -> terrainFlag.getFlag().equals(flag))
                    .forEach(terrainFlag -> {
                        terrainFlag.setValue(value);
                        val event = new TerrainUpdateFlagEvent(true, terrain, terrainFlag.getFlag(), terrainFlag.getValue());
                        Bukkit.getPluginManager().callEvent(event);
                        if (!event.isCancelled()) terrainFlagRepository.save(terrainFlag);
                    });
        });
    }

    @Override
    public List<TerrainFlag> getTerrainFlags(Terrain terrain) {
        return terrainFlagRepository.findByTerrain(terrain);
    }

    @Override
    public boolean getPvPFlag(Terrain terrain) {
        return Boolean.parseBoolean(getTerrainFlags(terrain).stream().filter(terrainFlag -> terrainFlag.getFlag().equals(Flag.PVP)).findFirst().get().getValue());
    }

    @Override
    public boolean getMobSpawnFlag(Terrain terrain) {
        return Boolean.parseBoolean(getTerrainFlags(terrain).stream().filter(terrainFlag -> terrainFlag.getFlag().equals(Flag.MOB)).findFirst().get().getValue());
    }

    @Override
    public boolean getEntryFlag(Terrain terrain) {
        return Boolean.parseBoolean(getTerrainFlags(terrain).stream().filter(terrainFlag -> terrainFlag.getFlag().equals(Flag.ENTRY)).findFirst().get().getValue());
    }

    @Override
    public String getGreetingFlag(Terrain terrain) {
        return getTerrainFlags(terrain).stream().filter(terrainFlag -> terrainFlag.getFlag().equals(Flag.GREETING)).findFirst().get().getValue();
    }

    @Override
    public String getFarewellFlag(Terrain terrain) {
        return getTerrainFlags(terrain).stream().filter(terrainFlag -> terrainFlag.getFlag().equals(Flag.FAREWELL)).findFirst().get().getValue();
    }
}