package com.gitlab.pk7r.aurora.terrain.util;

import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ChunkUtil {

    public static Chunk getChunk(String worldName, int x, int z) {
        return Bukkit.getWorlds()
                .stream()
                .filter(world -> world.getName().equalsIgnoreCase(worldName))
                .map(world -> world.getChunkAt(x, z)).toList().get(0);
    }

    public static Collection<Chunk> around(Chunk origin, int radius) {
        val world = origin.getWorld();

        int length = (radius * 2) + 1;
        Set<Chunk> chunks = new HashSet<>(length * length);

        int cX = origin.getX();
        int cZ = origin.getZ();

        for (int x = -radius; x <= radius; x++) {
            for (int z = -radius; z <= radius; z++) {
                chunks.add(world.getChunkAt(cX + x, cZ + z));
            }
        }
        return chunks;
    }

    public static Location getCenterChunkLocation(Chunk c) {
        Location center = new Location(c.getWorld(), c.getX() << 4, 64, c.getZ() << 4).add(8, 0, 8);
        center.setY(Objects.requireNonNull(center.getWorld()).getHighestBlockYAt(center) + 1);
        return center;
    }
}