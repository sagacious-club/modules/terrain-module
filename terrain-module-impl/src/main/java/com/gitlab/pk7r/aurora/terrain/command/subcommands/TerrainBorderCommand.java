package com.gitlab.pk7r.aurora.terrain.command.subcommands;

import com.gitlab.pk7r.aurora.service.DelayService;
import com.gitlab.pk7r.aurora.terrain.service.ChunkSerializationService;
import com.gitlab.pk7r.aurora.terrain.service.TerrainService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.val;
import org.bukkit.Particle;

import java.util.concurrent.TimeUnit;

public class TerrainBorderCommand {

    public static CommandAPICommand getCommand(TerrainService terrainService, ChunkSerializationService chunkSerializationService, DelayService delayService) {
        return new CommandAPICommand("borda")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.terrain.command.terreno.borda")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (terrainService.inTerrainsWorld(player)) {
                        player.spigot().sendMessage(new Message("&cVocê precisa estar no mundo dos terrenos para executar este comando.").formatted());
                        return;
                    }
                    if (!terrainService.isTerrain(chunkSerializationService.serializeChunk(player.getLocation().getChunk()), false)) {
                        player.spigot().sendMessage(new Message("&cVocê não está em um terreno.").formatted());
                        return;
                    }
                    if (!delayService.assertDelay(player, player.getName() + ":terrain-border", 180, TimeUnit.SECONDS)) {
                        val terrain = terrainService.getTerrainByChunk(chunkSerializationService.serializeChunk(player.getLocation().getChunk())).orElseThrow();
                        player.spigot().sendMessage(new Message("&aMostrando a borda do terreno que você está em cima por &e3 minutos&a.").formatted());
                        terrainService.showBorder(player, terrain, Particle.DRIP_LAVA, 180);
                    }
                });
    }
}
