<!DOCTYPE html>
<html lang="pt-BR">
<body>
    <a href="https://www.sagacious.club">
        <img alt="logo" align="right" src="https://i.postimg.cc/J4JfDD9c/Pandora-Logo-Circle.png" height="185" width="185"/>
    </a>
    <h1>Aurora Terrain</h1>
    <p><b>A module from AuroraAPI</b></p>
    <hr>
    <h2>Usage</h2>
</body>
</html>

First, make sure the repository is in your `pom.xml`:

```xml
<repository>
    <id>sagacious</id>
    <url>https://artifactory.nubraza.com/artifactory/libs-release-local/</url>
</repository>
```

Then, make sure the dependency is in your `pom.xml`:

```xml
<dependency>
    <groupId>com.gitlab.pk7r.aurora</groupId>
    <artifactId>terrain-module-api</artifactId>
    <version>1.0</version>
    <scope>provided</scope>
</dependency>
```
